variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "topic_name" {
  description = "name of the topic"
  type        = string
}

variable "fifo_enabled" {
  description = "enable fifo topic"
  type        = bool
  default     = false
}