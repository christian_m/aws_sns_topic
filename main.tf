resource "aws_sns_topic" "default" {
  name                        = var.topic_name
  fifo_topic                  = var.fifo_enabled
  content_based_deduplication = var.fifo_enabled

  tags = {
    env = var.environment
  }
}